// /**
//  * PIXI CANVAS
//  */
// var renderer = PIXI.autoDetectRenderer(512, 512);

// renderer.view.style.position = "absolute";
// renderer.view.style.display = "block";
// renderer.autoResize = true;
// renderer.resize(window.innerWidth, window.innerHeight);
// renderer.backgroundColor = 0xF3F3F3;

// document.body.appendChild(renderer.view);

// var stage = new PIXI.Container();
// var circle = new PIXI.Graphics();
// circle.lineStyle(2, 0xFF00FF);
// circle.drawCircle(0, 0, 10);
// circle.endFill();
// stage.addChild(circle);

// console.log(stage);

// document.addEventListener("keydown", (event) => {
//     socket.emit('keyEvent', { key: event.keyCode });
// });

// renderer.render(stage);

var socket = io();
var __players = {};
var __keysDown = {};
var __inputQueue = [];
var __allowedKeys = { 87: 'UP', 83: 'DOWN', 65: 'LEFT', 68: 'RIGHT' };
var __lastUpdateTime = Date.now();
var __frame = 0;

var __windowInnerWidth = window.innerWidth;
var __windowInnerHeight = window.innerHeight;


// Cross-browser support for requestAnimationFrame
requestAnimationFrame = window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.msRequestAnimationFrame || window.mozRequestAnimationFrame;

socket.on('connect', () => {

    console.log(socket);

    /**
     * Create canvas
     */
    var __canvas = document.createElement('canvas');
    var ctx = __canvas.getContext("2d");

    __canvas.id = "my-canvas";
    __canvas.style.background = "#f8f8f8";
    __canvas.width = __windowInnerWidth;
    __canvas.height = __windowInnerHeight;

    document.body.appendChild(__canvas);

    addEventListener("resize", (event) => {
        __canvas.width = __windowInnerWidth;
        __canvas.height = __windowInnerHeight;
    });

    /**
     * Events bind
     */
    addEventListener("keydown", (event) => {
        __keysDown[event.keyCode] = true;
    });

    addEventListener("keyup", (event) => {
        delete __keysDown[event.keyCode];
    });

    /**
     * Input
     */

    function SendInput(delta) {

        var actualInputKeys = [];
        var actualInputAngle = false;

        console.log(__keysDown);

        for (let key in __keysDown) {
            if (__allowedKeys[key] !== undefined) {
                actualInputKeys.push(__allowedKeys[key]);
            }
        }

        if (actualInputKeys.length) {
            var input = {
                'keys': actualInputKeys
            };

            socket.emit('input', input);
        }
    }

    /**
     * Render
     */
    function Render(delta) {

        // Clear canvas
        ctx.clearRect(0, 0, __canvas.width, __canvas.height);

        var translateX = __windowInnerWidth / 2 - __players[socket.id].posX;
        var translateY = __windowInnerHeight / 2 - __players[socket.id].posY;

        // Players
        for (let id in __players) {

            var playerPosX = 0;
            var playerPosY = 0;

            if (socket.id == id) {
                playerPosX += __windowInnerWidth / 2;
                playerPosY += __windowInnerHeight / 2;
            }
            else {
                playerPosX += __players[id].posX + translateX;
                playerPosY += __players[id].posY + translateY;
            }

            ctx.beginPath();
            ctx.arc(playerPosX, playerPosY, 20, 0, Math.PI * 2, false);
            ctx.fillStyle = "#cecece";
            ctx.fill();

            // ctx.moveTo(playerPosX, playerPosY);
            ctx.closePath();
        }

        // Render wall
        ctx.beginPath();
        ctx.moveTo(0 + translateX, 0 + translateY);
        ctx.lineTo(500 + translateX, 0 + translateY);
        ctx.lineTo(500 + translateX, 250 + translateY);
        ctx.lineTo(0 + translateX, 250 + translateY);
        ctx.lineTo(0 + translateX, 0 + translateY);
        ctx.stroke();
    }

    /**
     * Loop
     */
    function Loop() {

        var currentTime = Date.now();
        var delta = (currentTime - __lastUpdateTime) / 1000;
        __frame++;

        if (!_.isEmpty(__players)) {
            Render(delta);
            SendInput(delta);
        }

        __lastUpdateTime = currentTime;
        requestAnimationFrame(Loop);
    }

        // Call loop
    Loop();

    socket.on("update", (res) => {
        __players = res['players'];
    });
});

// socket.on('disconnect', () => {
//     console.log(`Player disconnect: ${socketID}`);
//     delete __players[socket.id];
// });