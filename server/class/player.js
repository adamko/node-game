const _ = require('lodash');

Player = class {

    constructor(id) {
        this.id = id;
        this.keys = [];
        this.queue = [];
        this.frame = 0;
        this.speed = 200;
        this.posX = 20;
        this.posY = 20;
        this.angle = 0;
        this.input = [];
    }

    updatePosition(delta) {
        for (let i in this.input) {
            if (_.indexOf(this.input.keys, 'UP') != -1)
                this.posY -= Math.round(this.speed * delta);

            if (_.indexOf(this.input.keys, 'DOWN') != -1)
                this.posY += Math.round(this.speed * delta);

            if (_.indexOf(this.input.keys, 'LEFT') != -1)
                this.posX -= Math.round(this.speed * delta);

            if (_.indexOf(this.input.keys, 'RIGHT') != -1)
                this.posX += Math.round(this.speed * delta);

            this.input = [];
        }
    }
};

module.exports = { Player };