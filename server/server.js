const path = require('path');
const http = require('http');
const express = require('express');
const socketIO = require('socket.io');
const _ = require('lodash');

// Class
const { Player } = require('./class/player');

const publicPath = path.join(__dirname, '../public');
const port = process.env.PORT || 3000;

var app = express();
var server = http.createServer(app);
var io = socketIO(server);

app.use(express.static(publicPath));

/**
 * NASTAVENIA
 */

var __UPDATE_PER_SEC = 60;
var __PLAYERS = {};
var __LAST_UPDATE = Date.now();

/**
 * IO CONNECT
 */

io.on("connection", (socket) => {

    let socketID = socket.id;

    console.log(`player connected: ${socketID}`);

    // Prida id hraca do pola
    __PLAYERS[socketID] = new Player(socketID);

    socket.on("input", (res) => {
        // console.log(res);
        // __PLAYERS[socketID].queue.push(res);
        // __PLAYERS[socketID].frame = res.frame;
        // __PLAYERS[socketID].keys = res.keys;
        // __PLAYERS[socketID].angle = res.angle;
        __PLAYERS[socketID].input = res;
    });

    // io.on("disconnect", () => {
    //     delete __PLAYERS[socketID];
    //     console.log(`Disconnect player: ${socketID}`);
    // });
});



/**
 * LOOP
 */

setInterval(() => {

    let currentTime = Date.now();
    let delta = (currentTime - __LAST_UPDATE) / 1000;

    for (let id in __PLAYERS) {
        __PLAYERS[id].updatePosition(delta);
    }

    io.emit("update", {
        'players': __PLAYERS
    });

    __LAST_UPDATE = currentTime;

}, 1000 / __UPDATE_PER_SEC);


server.listen(port, () => {
    console.log(`Server is up on ${port}`);
});